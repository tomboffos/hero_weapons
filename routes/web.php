<?php

use App\Http\Controllers\HeroesController;
use App\Http\Controllers\WeaponsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return \Inertia\Inertia::render('Welcome');
});

Route::get('/heroes', [HeroesController::class, 'index'])->name('heroes');
Route::get('/weapons', [WeaponsController::class, 'index'])->name('weapons');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
