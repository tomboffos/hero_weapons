<?php

namespace App\Providers;

use App\Domain\Hero\Contracts\HeroRepositoryInterface;
use App\Domain\Hero\Repositories\HeroRepositoryInterfaceImpl;
use App\Domain\Weapon\Contracts\WeaponRepositoryInterface;
use App\Domain\Weapon\Repositories\WeaponRepositoryInterfaceImpl;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(HeroRepositoryInterface::class, HeroRepositoryInterfaceImpl::class);
        $this->app->bind(WeaponRepositoryInterface::class, WeaponRepositoryInterfaceImpl::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
