<?php

namespace App\Http\Controllers;

use App\Domain\Hero\Contracts\HeroRepositoryInterface;
use Illuminate\Http\Request;
use Inertia\Inertia;

class HeroesController extends Controller
{
    protected $heroRepository;

    public function __construct(HeroRepositoryInterface $repository)
    {
        $this->heroRepository = $repository;
    }

    public function index()
    {
        return Inertia::render('HeroesTable',[
            'heroes' => $this->heroRepository->index()
        ]);
    }
}
