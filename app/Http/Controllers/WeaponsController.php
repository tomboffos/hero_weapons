<?php

namespace App\Http\Controllers;

use App\Domain\Weapon\Contracts\WeaponRepositoryInterface;
use Illuminate\Http\Request;
use Inertia\Inertia;

class WeaponsController extends Controller
{
    protected $weaponRepository;
    public function __construct(WeaponRepositoryInterface $repository)
    {
        $this->weaponRepository = $repository;
    }
    public function index()
    {
        return Inertia::render('WeaponsTable',[
            'weapons' => $this->weaponRepository->index()
        ]);
    }
}
