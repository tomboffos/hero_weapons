<?php

namespace App\Http\Resources\Weapon;

use Illuminate\Http\Resources\Json\ResourceCollection;

class WeaponResourceCollection extends ResourceCollection
{
    public $collects = WeaponResource::class;
}
