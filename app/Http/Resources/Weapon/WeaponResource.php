<?php

namespace App\Http\Resources\Weapon;

use Illuminate\Http\Resources\Json\JsonResource;

class WeaponResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request): array
    {
        return [
            'name' => $this->name,
            'damage' => $this->damage,
            'is_range' => $this->is_range,
            'sum_owners' => $this->getOwners()
        ];
    }
}
