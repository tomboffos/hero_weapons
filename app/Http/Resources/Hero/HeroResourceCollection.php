<?php

declare(strict_types=1);

namespace App\Http\Resources\Hero;

use Illuminate\Http\Resources\Json\ResourceCollection;

class HeroResourceCollection extends ResourceCollection
{
    public $collects = HeroResource::class;
}
