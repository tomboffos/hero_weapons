<?php

namespace App\Http\Resources\Hero;

use Illuminate\Http\Resources\Json\JsonResource;


class HeroResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'name' => $this->name,
            'health' => $this->health,
            'sum_damage' => $this->getDamage()
        ];
    }
}
