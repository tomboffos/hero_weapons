<?php


namespace App\Domain\Hero\Contracts;


interface HeroContract
{
    const NAME = 'name';
    const HEALTH = 'health';

    const FILLABLE = [
        self::NAME,
        self::HEALTH
    ];
}
