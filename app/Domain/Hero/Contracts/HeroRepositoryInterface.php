<?php


namespace App\Domain\Hero\Contracts;


interface HeroRepositoryInterface
{
    public function index();
}
