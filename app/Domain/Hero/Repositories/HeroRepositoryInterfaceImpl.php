<?php


namespace App\Domain\Hero\Repositories;


use App\Domain\Hero\Contracts\HeroRepositoryInterface;
use App\Http\Resources\Hero\HeroResource;
use App\Http\Resources\Hero\HeroResourceCollection;
use App\Models\Hero;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class HeroRepositoryInterfaceImpl implements HeroRepositoryInterface
{
    public function index() : JsonResource
    {
        return HeroResource::collection(Hero::query()->get());
    }
}
