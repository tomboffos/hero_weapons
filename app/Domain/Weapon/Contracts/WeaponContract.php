<?php


namespace App\Domain\Weapon\Contracts;


interface WeaponContract
{
    const NAME = 'name';
    const DAMAGE = 'damage';
    const IS_RANGE = 'is_range';

    const FILLABLE = [
        self::NAME,
        self::DAMAGE,
        self::IS_RANGE
    ];
}
