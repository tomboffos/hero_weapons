<?php


namespace App\Domain\Weapon\Contracts;


interface WeaponRepositoryInterface
{
    public function index();
}
