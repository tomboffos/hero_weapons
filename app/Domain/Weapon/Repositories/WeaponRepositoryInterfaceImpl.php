<?php


namespace App\Domain\Weapon\Repositories;


use App\Http\Resources\Weapon\WeaponResource;
use App\Http\Resources\Weapon\WeaponResourceCollection;
use App\Models\Weapon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class WeaponRepositoryInterfaceImpl implements \App\Domain\Weapon\Contracts\WeaponRepositoryInterface
{
    public function index() : JsonResource
    {
        return WeaponResource::collection(Weapon::query()->get());
    }
}
