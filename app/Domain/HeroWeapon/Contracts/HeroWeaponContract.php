<?php


namespace App\Domain\HeroWeapon\Contracts;


interface HeroWeaponContract
{
    const HERO_ID = 'hero_id';
    const WEAPON_ID = 'weapon_id';

    const FILLABLE = [
        self::HERO_ID,
        self::WEAPON_ID
    ];
}
