<?php

namespace App\Observers;

use App\Jobs\EmailAdminJob;
use App\Models\Hero;
use Illuminate\Support\Facades\Log;

class HeroObserver
{
    /**
     * Handle the Hero "created" event.
     *
     * @param  \App\Models\Hero  $hero
     * @return void
     */
    public function created(Hero $hero)
    {
        dispatch(new EmailAdminJob('admin@test.com'));
    }

    /**
     * Handle the Hero "updated" event.
     *
     * @param  \App\Models\Hero  $hero
     * @return void
     */
    public function updated(Hero $hero)
    {
        //
    }

    /**
     * Handle the Hero "deleted" event.
     *
     * @param  \App\Models\Hero  $hero
     * @return void
     */
    public function deleted(Hero $hero)
    {
        //
    }

    /**
     * Handle the Hero "restored" event.
     *
     * @param  \App\Models\Hero  $hero
     * @return void
     */
    public function restored(Hero $hero)
    {
        //
    }

    /**
     * Handle the Hero "force deleted" event.
     *
     * @param  \App\Models\Hero  $hero
     * @return void
     */
    public function forceDeleted(Hero $hero)
    {
        //
    }
}
