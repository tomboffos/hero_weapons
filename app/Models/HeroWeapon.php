<?php

namespace App\Models;

use App\Domain\HeroWeapon\Contracts\HeroWeaponContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Pivot class HeroWeapon
 * @property int $hero_id
 * @property int $weapon_id
 */

class HeroWeapon extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = HeroWeaponContract::FILLABLE;
}
