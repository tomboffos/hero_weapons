<?php

namespace App\Models;

use App\Domain\Hero\Contracts\HeroContract;
use App\Domain\Weapon\Contracts\WeaponContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Hero
 * @property int $id
 * @property string $name
 * @property double $health
 */

class Hero extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = HeroContract::FILLABLE;

    public function weapons() : BelongsToMany
    {
        return $this->belongsToMany(Weapon::class, 'hero_weapons', 'hero_id', 'weapon_id');
    }

    public function getDamage() : int
    {
        return $this->weapons()->sum(WeaponContract::DAMAGE);
    }
}
