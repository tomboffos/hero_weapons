<?php

namespace App\Models;

use App\Domain\Hero\Contracts\HeroContract;
use App\Domain\Weapon\Contracts\WeaponContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Hero
 * @property string $name
 * @property double $health
 * @property boolean $is_range
 */

class Weapon extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = WeaponContract::FILLABLE;

    public function heroes() : BelongsToMany
    {
        return $this->belongsToMany(Hero::class, 'hero_weapons', 'weapon_id', 'hero_id');
    }

    public function getOwners() : int
    {
        return $this->heroes()->count();
    }
}
