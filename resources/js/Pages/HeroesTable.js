import React from 'react';
import Authenticated from '@/Layouts/Authenticated';
import {Head} from '@inertiajs/inertia-react';
import {Table} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';

export default function HeroesTable({heroes}) {
    return (

        <div className={'container'}>
            <div className="row">
                <Table striped bordered hover className={'mt-5'}>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Health</th>
                        <th>Sum damage</th>
                    </tr>
                    </thead>
                    <tbody>

                    {heroes.data.map((hero, index) =>
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{hero.name}</td>
                            <td>{hero.health}</td>
                            <td>{hero.sum_damage}</td>
                        </tr>
                    )}

                    </tbody>
                </Table>
            </div>
        </div>
    );
}
