import React from 'react';
import { Link, Head } from '@inertiajs/inertia-react';
import 'bootstrap/dist/css/bootstrap.min.css';

export default function Welcome(props) {
    return (
        <>
            <div className="container">
                <div className="row mt-5">
                    <div className="col-lg-6">
                        <a href={'/heroes'} className={'btn btn-primary'}>Heroes</a>
                        <a href={'/weapons'} className={'btn btn-primary'}>Weapons</a>
                    </div>
                </div>
            </div>
        </>
    );
}
