import {Table} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';

export default function WeaponsTable({weapons}) {
    return (
        <div className={'container'}>
            <div className="row">
                <Table striped bordered hover className={'mt-5'}>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Damage</th>
                        <th>Sum owners</th>
                        <th>Is range</th>
                    </tr>
                    </thead>
                    <tbody>

                    {weapons.data.map((weapon, index) =>
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{weapon.name}</td>
                            <td>{weapon.damage}</td>
                            <td>{weapon.sum_owners}</td>
                            <td>{weapon.is_range ? 'no' : 'yes'}</td>

                        </tr>
                    )}

                    </tbody>
                </Table>
            </div>
        </div>
    )
}
